package com.excel;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import jxl.write.WriteException;

public class Scheduler 
//extends TimerTask 
{
	
	public void run() {
		System.out.println("Scheduler start:"+new Date().toString());
		try {
			WriteExcel.createFile();
			//SendFileEmail.sendEmail("b.struzanskiy@gmail.com");
			SendFileEmail.sendEmail("bohdanstr@trans-service-1.com.ua");
			//SendFileEmail.sendEmail("markevych_vasyl@trans-service-1.com.ua");
			//SendFileEmail.sendEmail("vasylyshyn_andriy@trans-service-1.com.ua");
			SendFileEmail.sendEmail("sarabun_jurij@trans-service-1.com.ua");
			System.out.println("Scheduler finished:"+new Date().toString());
		} catch (WriteException | IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

	public static void main(String[] args) {
		new Scheduler().run();
		/*
		//Timer timer = new Timer();
		//timer.schedule(new Scheduler(), 0, 900000);
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 8);
		today.set(Calendar.MINUTE, 10);
		today.set(Calendar.SECOND, 0);
		System.out.println(today.getTime());
		Timer timer = new Timer();
		timer.schedule(new Scheduler(), today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)); // 60*60*24*100 = 8640000ms
		*/
	
	}

}
