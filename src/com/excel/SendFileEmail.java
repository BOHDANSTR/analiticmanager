package com.excel;

import java.io.UnsupportedEncodingException;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class SendFileEmail {

   public static void sendEmail(String to) throws UnsupportedEncodingException {     
      // Sender's email ID needs to be mentioned
	  
      String user = "cooltrackservice007@gmail.com";
      String pass = "COOLcoolCool23082017";
      String host = "smtp.gmail.com";

      // Get system properties
      Properties  properties = System.getProperties();
      properties.put("mail.smtp.user", user);
      properties.put("mail.smtp.host", host);
      properties.put("mail.smtp.port",  "465");
      properties.put("mail.smtp.socketFactory.port",  "465");
      properties.put("mail.smtp.starttls.enable","true");
      properties.put("mail.smtp.auth", "true");
      properties.put("mail.smtp.debug", "true");
      properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
      properties.put("mail.smtp.socketFactory.fallback", "false");

      Session session = Session.getDefaultInstance(properties, 
    		    new javax.mail.Authenticator(){
    		        protected PasswordAuthentication getPasswordAuthentication() {
    		            return new PasswordAuthentication(user, pass);// Specify the Username and the PassWord
    		        }
    		});

      // Get the default Session object.

      try {
         // Create a default MimeMessage object.
         MimeMessage message = new MimeMessage(session);

         // Set From: header field of the header.
         message.setFrom(new InternetAddress(user,"Analitics server"));
	

         // Set To: header field of the header.
         message.addRecipient(Message.RecipientType.TO,new InternetAddress(to,"analitik"));

         // Set Subject: header field
         Date date = new Date();
         message.setSubject("Статиситка зупинок "+((date.getDate()<10)? "0"+date.getDate():date.getDate())+"."+((date.getMonth()<9)? "0"+(date.getMonth()+1):(date.getMonth()+1))+"."+(date.getYear()+1900));

         // Create the message part 
         BodyPart messageBodyPart = new MimeBodyPart();

         // Fill the message
         messageBodyPart.setText("");
         
         // Create a multipar message
         Multipart multipart = new MimeMultipart();

         // Set text message part
         multipart.addBodyPart(messageBodyPart);

         // Part two is attachment
         messageBodyPart = new MimeBodyPart();
         String filename = "Statistic of stops "+((date.getDate()<10)? "0"+date.getDate():date.getDate())+"."+((date.getMonth()<9)? "0"+(date.getMonth()+1):(date.getMonth()+1))+"."+(date.getYear()+1900+".xls");
         DataSource source = new FileDataSource(filename);
         messageBodyPart.setDataHandler(new DataHandler(source));
         messageBodyPart.setFileName(filename);
         multipart.addBodyPart(messageBodyPart);

         // Send the complete message parts
         message.setContent(multipart );

         // Send message
         Transport.send(message);
         System.out.println("Sent message successfully....");
      }catch (MessagingException mex) {
         mex.printStackTrace();
      }
   }
   
}