﻿CREATE OR REPLACE FUNCTION ts1.zone_archive_report(
    IN startdate text,
    IN stopdate text)
  RETURNS TABLE(obj_id_p integer, object_name_p text, time_p text, zone_name text) AS
$BODY$
-- example how execute:
-- select *from ts1.zone_archive_report('01.01.2019 00:00:00','06.02.2019 00:00:00');
DECLARE
	first_point point = point(46.34882987616019,30.592214191665658); --транс сервіс
	first_point_size integer:=61;
	first_point_name text:='Зона 1';
	
	second_point point = point(46.3603816266997,30.585063422431954); 
	second_point_size integer:=30;
	second_point_name text:='Зона 2';

	third_point point = point(46.932852588846835,31.6039559059448);
	third_point_size integer:=250;
	third_point_name text:='Зона 3';
	
	sugar_point point = point(48.70973212423268,32.721909284591675);
	sugar_point_size integer:=42;
	sugar_point_name text:='Цукор';
	
	nikopol_point point = point(47.584170239208824,34.37581370304109);
	nikopol_point_size integer:=54;
	nikopol_point_name text:='Нікополь';
	
	
	geom_a text;
	geom_b text;
	cur_rec RECORD;
	current_object RECORD;
	distance double precision;

BEGIN
<<objects_loop>>
FOR current_object IN select objects.obj_id, objects.object_name from objects,groups where objects.grp_id = groups.grp_id and (grp_descr = 'Самосвал' or grp_descr = 'Цистерна') order by obj_id
LOOP
BEGIN

	<<search_location>>
	FOR cur_rec IN select degree_position[0] as x,degree_position[1] as y,datetime from ts1.select_from_m_geo_position(startdate,stopdate,current_object.obj_id)
	LOOP
	BEGIN
		geom_a:='POINT('||first_point[1]||' '||first_point[0]||')';
		geom_b:='POINT(' || cur_rec.x || ' ' || cur_rec.y || ')';
		distance:=ST_Distance_Sphere(ST_GeomFromText(geom_a, 4030), ST_GeomFromText(geom_b, 4030));
		IF distance<=first_point_size THEN 
			BEGIN
				obj_id_p:= current_object.obj_id;
				object_name_p:= current_object.object_name;
				time_p:= to_char(to_timestamp((cur_rec.datetime)), 'DD.MM.YYYY HH24:MI:SS');
				zone_name:=first_point_name;
				RAISE NOTICE 'accident was found obj_id_p[%],object_name_p[%],time_p[%],zone_p[%]',obj_id_p,object_name_p,time_p,zone_name;				
				return next;
			END;
		END IF;
		
		geom_a:='POINT('||second_point[1]||' '||second_point[0]||')';
		geom_b:='POINT(' || cur_rec.x || ' ' || cur_rec.y || ')';
		distance:=ST_Distance_Sphere(ST_GeomFromText(geom_a, 4030), ST_GeomFromText(geom_b, 4030));
		IF distance<=second_point_size THEN 
			BEGIN
				obj_id_p:= current_object.obj_id;
				object_name_p:= current_object.object_name;
				time_p:= to_char(to_timestamp((cur_rec.datetime)), 'DD.MM.YYYY HH24:MI:SS');
				zone_name:=second_point_name;
				RAISE NOTICE 'accident was found obj_id_p[%],object_name_p[%],time_p[%],zone_p[%]',obj_id_p,object_name_p,time_p,zone_name;				
				return next;
			END;
		END IF;

		
		geom_a:='POINT('||third_point[1]||' '||third_point[0]||')';
		geom_b:='POINT(' || cur_rec.x || ' ' || cur_rec.y || ')';
		distance:=ST_Distance_Sphere(ST_GeomFromText(geom_a, 4030), ST_GeomFromText(geom_b, 4030));
		IF distance<=third_point_size THEN 
			BEGIN
				obj_id_p:= current_object.obj_id;
				object_name_p:= current_object.object_name;
				time_p:= to_char(to_timestamp((cur_rec.datetime)), 'DD.MM.YYYY HH24:MI:SS');
				zone_name:=third_point_name;
				RAISE NOTICE 'accident was found obj_id_p[%],object_name_p[%],time_p[%],zone_p[%]',obj_id_p,object_name_p,time_p,zone_name;				
				return next;
			END;
		END IF;
		
		geom_a:='POINT('||sugar_point[1]||' '||sugar_point[0]||')';
		geom_b:='POINT(' || cur_rec.x || ' ' || cur_rec.y || ')';
		distance:=ST_Distance_Sphere(ST_GeomFromText(geom_a, 4030), ST_GeomFromText(geom_b, 4030));
		IF distance<=sugar_point_size THEN 
			BEGIN
				obj_id_p:= current_object.obj_id;
				object_name_p:= current_object.object_name;
				time_p:= to_char(to_timestamp((cur_rec.datetime)), 'DD.MM.YYYY HH24:MI:SS');
				zone_name:=sugar_point_name;
				RAISE NOTICE 'accident was found obj_id_p[%],object_name_p[%],time_p[%],zone_p[%]',obj_id_p,object_name_p,time_p,zone_name;				
				return next;
			END;
		END IF;
		
		geom_a:='POINT('||nikopol_point[1]||' '||nikopol_point[0]||')';
		geom_b:='POINT(' || cur_rec.x || ' ' || cur_rec.y || ')';
		distance:=ST_Distance_Sphere(ST_GeomFromText(geom_a, 4030), ST_GeomFromText(geom_b, 4030));
		IF distance<=nikopol_point_size THEN 
			BEGIN
				obj_id_p:= current_object.obj_id;
				object_name_p:= current_object.object_name;
				time_p:= to_char(to_timestamp((cur_rec.datetime)), 'DD.MM.YYYY HH24:MI:SS');
				zone_name:=nikopol_point_name;
				RAISE NOTICE 'accident was found obj_id_p[%],object_name_p[%],time_p[%],zone_p[%]',obj_id_p,object_name_p,time_p,zone_name;				
				return next;
			END;
		END IF;

	END;
	END LOOP search_location;

END;
END LOOP objects_loop;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ts1.zone_archive_report(text, text)
  OWNER TO ts_admin;



CREATE OR REPLACE FUNCTION ts1.zone_archive_report(
    IN startdate text,
    IN stopdate text,
	IN ponint_p point,
	IN radius_p integer,
	IN name_p text,
	IN grp_descr_arr text[]
	)
  RETURNS TABLE(obj_id_p integer, object_name_p text, time_p text, zone_name text) AS
$BODY$
-- example how execute:
-- select *from ts1.zone_archive_report('01.01.2019 00:00:00','06.02.2019 00:00:00',point(46.932852588846835,31.6039559059448),250,'зона 1',array['Самосвал','Цистерна']);
DECLARE
	geom_a text;
	geom_b text;
	cur_rec RECORD;
	current_object RECORD;
	distance double precision;

BEGIN
<<objects_loop>>
FOR current_object IN select objects.obj_id, objects.object_name from objects,groups where objects.grp_id = groups.grp_id and grp_descr = ANY (grp_descr_arr) order by obj_id
LOOP
BEGIN

	<<search_location>>
	FOR cur_rec IN select degree_position[0] as x,degree_position[1] as y,datetime from ts1.select_from_m_geo_position(startdate,stopdate,current_object.obj_id)
	LOOP
	BEGIN
		geom_a:='POINT('||ponint_p[1]||' '||ponint_p[0]||')';
		geom_b:='POINT(' || cur_rec.x || ' ' || cur_rec.y || ')';
		distance:=ST_Distance_Sphere(ST_GeomFromText(geom_a, 4030), ST_GeomFromText(geom_b, 4030));
		IF distance<=radius_p THEN 
			BEGIN
				obj_id_p:= current_object.obj_id;
				object_name_p:= current_object.object_name;
				time_p:= to_char(to_timestamp((cur_rec.datetime)), 'DD.MM.YYYY HH24:MI:SS');
				zone_name:=name_p;
				RAISE NOTICE 'accident was found obj_id_p[%],object_name_p[%],time_p[%],zone_p[%]',obj_id_p,object_name_p,time_p,zone_name;				
				return next;
			END;
		END IF;

	END;
	END LOOP search_location;

END;
END LOOP objects_loop;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ts1.zone_archive_report(text, text)

  OWNER TO ts_admin;