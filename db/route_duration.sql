CREATE OR REPLACE FUNCTION ts1.route_duration_report(startdate text, stopdate text, direction integer)
RETURNS TABLE( obj_id_p integer, object_name_p text,  direction_p text, duration_p integer, start_time_p integer, finish_time_p integer) AS
$BODY$
DECLARE
control_points point[]:=ARRAY[
							point(49.79122065157784,23.935670334779374), --транс сервіс
							point(49.777960, 23.976018), --виїзд кульпарківська
							point(49.769079, 24.014416), --виїзд стрийська
							point(49.768732, 24.106673), --виїзд зелена
							point(49.817630, 24.159545), --виїзд виннники
							point(49.840291, 24.167308), --виїзд підбірці
							point(49.873547, 24.164530), --виїзд сороки-львівські
							point(49.896400, 24.138321)  --виїзд на київську трасу	
						]; --числення масиву з 1 а точки з 0
zone_radius integer:=250;
route_length integer:=30500; 
geom_a text;
geom_b text;
cur_rec RECORD;
cur_obj RECORD;
distance double precision;
next_distance double precision:=-1;
rided_distance double precision;
object_in_zone boolean:=false;

start_zone integer;
finish_zone integer;

curent_zone integer;
optimized_startdate text;
optimized_stopdate text;
search_is_not_finished boolean:=true;
max_time integer:=  3*3600; --максимальний час для проходження маршуту
BEGIN

IF direction=-1 THEN 
	BEGIN
		start_zone:=array_length(control_points, 1);
		finish_zone:=1;
	END;
ELSE 
	BEGIN
		start_zone:=1;
		finish_zone:=array_length(control_points, 1);
	END;
END IF;
curent_zone:=start_zone;


<<objects_loop>>
FOR cur_obj IN 
select obj_id,object_name from objects,groups where objects.grp_id = groups.grp_id and groups.grp_descr in ('Тент','Цистерна','Самосвал') order by groups.grp_descr,object_name 
--select obj_id,object_name from objects where obj_id=2524
LOOP
BEGIN
optimized_startdate:=startdate;
optimized_stopdate:=startdate;

<<global_search_loop>>
WHILE optimized_stopdate::abstime::integer <= stopdate::abstime::integer LOOP
BEGIN

optimized_stopdate:=to_char(to_timestamp((optimized_startdate::abstime::integer + max_time)), 'DD.MM.YYYY HH24:MI:SS');
--RAISE NOTICE 'optimized_startdate:% optimized_stopdate:%',optimized_startdate,optimized_stopdate;
<<search_loop>>
FOR cur_rec IN select degree_position[0] as x,degree_position[1] as y ,datetime from ts1.select_from_m_geo_position(optimized_startdate,optimized_stopdate,cur_obj.obj_id) LOOP
	BEGIN
		geom_a:='POINT(' || (control_points[curent_zone])[1] || ' ' || (control_points[curent_zone])[0] || ')';
		geom_b:='POINT(' || cur_rec.x || ' ' || cur_rec.y || ')';
		distance:=ST_Distance_Sphere(ST_GeomFromText(geom_a, 4030), ST_GeomFromText(geom_b, 4030));
		
		
		--RAISE NOTICE 'distance:%, datetime:%',distance,to_char(to_timestamp((cur_rec.datetime)), 'DD.MM.YYYY HH24:MI:SS');
		IF distance>10000 THEN 
			BEGIN
				optimized_startdate:=to_char(to_timestamp((optimized_startdate::abstime::integer + distance/28)), 'DD.MM.YYYY HH24:MI:SS'); --distance/28 це час для подолання відстанні зі швидкістю 100км/год
				EXIT search_loop;
			END;
		END IF;
		
	
		--RAISE NOTICE 'curent_zone[%]',curent_zone;
		
		IF distance<zone_radius THEN object_in_zone:=true; END IF;
		IF object_in_zone=true and distance>zone_radius THEN
			BEGIN	
				object_in_zone:=false;
				--RAISE NOTICE 'found zone[%],datetime[%],obj_id[%]',curent_zone,cur_rec.datetime,cur_obj.obj_id;
				IF curent_zone=start_zone THEN start_time_p:=cur_rec.datetime; END IF;
				IF curent_zone=finish_zone THEN 
					BEGIN
						finish_time_p:=cur_rec.datetime;
					    rided_distance:= ts1.distance(to_char(to_timestamp(start_time_p), 'DD.MM.YYYY HH24:MI:SS'),to_char(to_timestamp(finish_time_p), 'DD.MM.YYYY HH24:MI:SS'),cur_obj.obj_id);
						
						if (rided_distance<=route_length) THEN 
							BEGIN
								obj_id_p:=cur_obj.obj_id;
								object_name_p:=cur_obj.object_name;
								IF start_zone<finish_zone THEN direction_p:='forward'; ELSE direction_p:='reverse'; END IF;
								duration_p:=finish_time_p-start_time_p;
								optimized_startdate:=to_char(to_timestamp((finish_time_p)), 'DD.MM.YYYY HH24:MI:SS');
								RAISE NOTICE 'found route start_time_p[%],finish_time_p[%],duration_p[%],direction[%],obj_d[%]',
									to_char(to_timestamp(start_time_p), 'DD.MM.YYYY HH24:MI:SS'),
									to_char(to_timestamp(finish_time_p), 'DD.MM.YYYY HH24:MI:SS'),
									((duration_p/ 3600) || ':'::text) || (duration_p% 3600 / 60),
									direction_p,
									cur_obj.obj_id;
								return next;
							END;
							END IF;
						start_time_p:=0;
						finish_time_p:=0;
						curent_zone:=start_zone;
						
					END;
				ELSE
					IF start_zone<finish_zone THEN curent_zone:=curent_zone+1; ELSE curent_zone:=curent_zone-1; END IF;
				END IF;
				geom_a:='POINT(' || (control_points[curent_zone])[1] || ' ' || (control_points[curent_zone])[0] || ')';
				geom_b:='POINT(' || cur_rec.x || ' ' || cur_rec.y || ')';
				next_distance:=ST_Distance_Sphere(ST_GeomFromText(geom_a, 4030), ST_GeomFromText(geom_b, 4030));
		
				optimized_startdate:=to_char(to_timestamp((cur_rec.datetime)), 'DD.MM.YYYY HH24:MI:SS');
				EXIT search_loop;
			END;
		END IF;
		

		
		IF next_distance<>(-1) and next_distance<distance THEN 
			BEGIN
				curent_zone:=start_zone;
				--RAISE NOTICE 'RESET next_distance:%',next_distance;
			END;	
		END IF;

		
	END;
END LOOP search_loop;
IF (optimized_stopdate::abstime::integer - optimized_startdate::abstime::integer)= max_time THEN optimized_startdate:=optimized_stopdate; END IF; --якщо нічого не найшли йдемо до наступного куска
END;
END LOOP  global_search_loop;
END;

END LOOP objects_loop;

END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


select obj_id_p, object_name_p, direction_p,
((duration_p/ 3600) || ':'::text) || (duration_p% 3600 / 60) AS duration_value,
to_char(to_timestamp(start_time_p), 'DD.MM.YYYY HH24:MI:SS'),
to_char(to_timestamp(finish_time_p), 'DD.MM.YYYY HH24:MI:SS'),duration_p,start_time_p,finish_time_p
from  ts1.route_duration_report('01.03.2018','01.05.2018',-1);
select obj_id_p, object_name_p, direction_p,
((duration_p/ 3600) || ':'::text) || (duration_p% 3600 / 60) AS duration_value,
to_char(to_timestamp(start_time_p), 'DD.MM.YYYY HH24:MI:SS'),
to_char(to_timestamp(finish_time_p), 'DD.MM.YYYY HH24:MI:SS'),duration_p,start_time_p,finish_time_p
from  ts1.route_duration_report('01.03.2018','01.05.2018',1);