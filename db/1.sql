CREATE OR REPLACE FUNCTION ts1.count_track_statistic(
    IN startdate text,
    IN enddate text)
  RETURNS TABLE(object_id integer, object_name_p text, object_description_p text, status_p text,period_p text, information text, code integer) AS
$BODY$
DECLARE
 rws RECORD;
 --object_id integer;
 object_name_text text;
 groups_description_text text;
 status_text text;
 startdate_string text;
 enddate_string text;
  BEGIN
  FOR object_id, object_name_text, groups_description_text,status_text IN  select obj_id,object_name, groups.grp_descr,
  (select accident_hint from ts1.current_device_accident where current_device_accident.obj_id= objects.obj_id limit 1) as status from objects,groups where objects.grp_id = groups.grp_id
  and groups.grp_descr in ('����','��������','��������') order by groups.grp_descr,object_name LOOP

  object_name_p:= object_name_text;
  object_description_p:= groups_description_text;
  status_p:=status_text;
  period_p:='';
  
  select  to_char((distance/1000.0)::numeric, '999990D99') as distance_all from ts1.distance(startdate||' 08:00:00',enddate||' 08:00:00',object_id) INTO rws;
  information = rws.distance_all;
  period_p:='08:00-08:00';
  code:=0;
  RETURN next; 
  --object_name_p:='';
  --object_description_p:='';
  --status_p:='';
  
  select  to_char((distance/1000.0)::numeric, '999990D99') as distance_evening from ts1.distance(startdate||' 19:00:00',startdate||' 23:59:00',object_id) INTO rws;
  information = rws.distance_evening;
  period_p:='19:00-23:59';

  code:=1;
  RETURN next; 
  
  select  to_char((distance/1000.0)::numeric, '999990D99') as distance_morning from ts1.distance(enddate||' 06:00:00',enddate||' 08:00:00',object_id) INTO rws;
  information = rws.distance_morning;
  period_p:='06:00-08:00';
  code:=2;
  RETURN next; 
  
  code:=3;
  information:=' ';
  period_p:=' ';
  RETURN next;
  --round(duration_of_stops_double, 2)
  --ts_distance
  END LOOP;
RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  
  
  CREATE OR REPLACE FUNCTION ts1.distance_per_date(date_p text)
RETURNS TABLE( distance_obj_id integer, distance_per_day double precision) as
$BODY$
BEGIN
FOR distance_obj_id, distance_per_day in
select obj_id, ts1.distance(to_char((($1||' 08:00:00')::timestamp - interval'1 day '), 'DD.MM.YYYY HH24:MI:SS'::text),$1||' 08:00:00',obj_id) as distance_day 
from objects, groups where objects.grp_id = groups.grp_id and grp_descr in ('��������','��������','����', '�����������')
loop 
RETURN next;
END LOOP;
RETURN;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100
ROWS 1000;
