create or replace view speed_data as 
select object_name,obj_descr,grp_name, to_char(to_timestamp(datetime), 'DD.MM.YYYY HH24:MI:SS'::text) as datetime_txt,datetime,speed
from objects inner join groups on objects.grp_id = groups.grp_id
inner join m_geo_position on objects.obj_id = m_geo_position.obj_id
where grp_name in('Транс-сервіс-1', 'Оліяр', 'ОЕЗ','Виїздні бригади') or grp_descr in('Цистерна','Самосвал' ,'Тент'); 
grant select on speed_data to sukach_jurij;

select object_name,obj_descr,grp_name,datetime_txt,speed from speed_data
where datetime > '09.10.2020 12:15'::abstime::integer;